#!/usr/bin/env bash
pid=`ps -ef|grep java|grep guns|awk '{print $2}'`
echo "guns Id list :$pid"
if [ "$pid" = "" ]
then
  echo "no guns pid alive"
else
  kill -9 $pid
fi

java -jar /root/.jenkins/workspace/guns/target/guns-1.0.0.jar&
